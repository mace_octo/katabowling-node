const assert = require('assert');
const expect = require('chai').expect;
const sinon = require('sinon')
const Score = require('../app/project.js');
const Game = require('../app/game.js');

describe('Score Test', () => {

    context('When no strike or spare', () => {
        it('should return the score of one value', () => {
            //Given
            let score = new Score();
            let game = [[1,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]];
            let expected_result = 1;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })

        it('should return total score of multiple values', () => {
            //Given
            let score = new Score();
            let game = [[1,0],[0,6],[0,0],[3,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]];
            let expected_result = 10;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })

    })

    context('When spare and no strike', () => {
        it('should return double value of the first play from the next frame', () => {
            //Given
            let score = new Score();
            let game = [[5,5],[1,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]];
            let expected_result = 12;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })
    })

    context('When strike and no spare', () => {
        it('should return double value of the next frame', () => {
            //Given
            let score = new Score();
            let game = [[10],[3,1],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]];
            let expected_result = 18;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })
    })

    context('When multiple strike and no spare', () => {
        it('should return double value of the multiple frame', () => {
            //Given
            let score = new Score();
            let game = [[10],[10],[2,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]];
            let expected_result = 36;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })

        it('should return double value of the multiple frame', () => {
            //Given
            let score = new Score();
            let game = [[10],[10],[10],[10],[5,0],[0,0],[0,0],[0,0],[0,0],[0,0]];
            let expected_result = 105;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })
    })

    context('When there is a bonus after strike or spare', () => {
        it('should return total score with bonus value for a spare', () => {
            //Given
            let score = new Score();
            let game = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [6, 4], [6]];
            let expected_result = 16;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })

        it('should return total score with bonus value for a strike', () => {
            //Given
            let score = new Score();
            let game = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [10], [6, 3]];
            let expected_result = 19;
            //When
            let scoreScore = score.computeScore(game);
            //Then
            expect(scoreScore).to.equal(expected_result);
        })
    })

    context('Throw function', () => {

        it('Should return max of 10', () => {
            //Given
            let score = new Score();
            let value = 1;
            sinon.stub(Math, 'random').returns(value)//STUB
            let expected_result = 10;
            let maxValue = 10;
            //When
            let result = score.throw(maxValue)
            //Then
            expect(result).to.be.at.most(expected_result);
        })
    })

    context('Play frame function', () => {

        it('should return array of length 2', () => {
            //Given
            let score = new Score();
            //When
            let resultFrame = score.playFrame();
            //Then
            expect(resultFrame).to.be.instanceOf(Array).and.to.have.lengthOf(2);
        })

        it('should return maximum of 10 pins per frame', () => {
            //Given
            let score = new Score();
            let value = 1;
            /*sinon.createStubInstance(Math.random, () => {
                random: sinon.stub().returns(value)
            });*/

            /*sinon.createStubInstance(Math.random, () => {
                return value
            });*/

             sinon.stub(Math, 'random').returns(value)

            let expected_result = 10;
            //When
            let result = score.playFrame()
            //Then
            expect(result[0] + result[1]).to.be.at.most(expected_result);
        })
    })

})
