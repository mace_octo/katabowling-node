const assert = require('assert');
const expect = require('chai').expect;
const sinon = require('sinon')
const Score = require('../app/project.js');
const Game = require('../app/game.js');
const Display = require('../app/display.js');

describe('Display Test', () => {

    context('Display scoreboard', () => {

        it('should return the display scoreboard', () => {
            //Given
            let display = new Display();
            let expected_result = "[[0,0],[0,0],[0,0]]";
            let scorebaord = [[0, 0], [0, 0], [0, 0]]
            //When
            let result = display.displayScoreboard(scorebaord)
            //Then
            expect(result).to.equal(expected_result);
        })
    })

})
