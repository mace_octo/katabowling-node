class Score {

    constructor(){
    }

    reducer = (accumulator, currentValue) => accumulator + currentValue;

    isSpare(frame){
        return frame.reduce(this.reducer) === 10 && frame.length > 1;
    }

    isStrike(frame){
        return frame[0] === 10;
    }

    randomFrame(){
        let firstThrow = Math.floor(Math.random() * Math.floor(10));
        let secondThrow = 0;
        return [firstThrow, secondThrow];
    }

    computeScore(game){
        let total_score = 0;

        //if(game.length)

        for (let turn = 0; turn < game.length; turn++)
        {
            total_score += game[turn].reduce(this.reducer);

            if (turn < 9){
                if (this.isStrike(game[turn]))
                {
                    total_score += game[turn + 1].reduce(this.reducer);
                    if (this.isStrike(game[turn + 1]))
                    {
                        total_score += game[turn + 2][0];
                    }
                }
                else if (this.isSpare(game[turn])) {
                    total_score += game[turn+1][0];
                }
            }
        }

        return total_score;
    }
}

module.exports = Score
